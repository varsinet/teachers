<?php
/**
 * Created by PhpStorm.
 * User: samparsky
 * Date: 4/17/16
 * Time: 11:25 AM
 */

namespace App\Providers;


use App\Helpers\MakeRequest;
use Illuminate\Support\ServiceProvider;

class MakeRequestServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('App\Helpers\Contracts\MakeRequestContract',function(){
           return new MakeRequest();
       });
    }
    public function provides()
    {
        return ['App\Helpers\Contracts\MakeRequestContract'];
    }
}