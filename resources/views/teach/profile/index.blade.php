@extends('teach/master')
@section('content')
    <section id="main-content">
        <section class="wrapper" style="margin-top: 60px;">
            <div class="row state-overview">
                <div class="state col-lg-12">
                    <div class="row">
                        <div class="col-lg-offset-4 col-lg-4 col-sm-6 panel">
                            <img src="{{ URL::asset('img/avatar1_small.png') }}" style="margin-left: auto;
                            margin-right: auto;padding: 20px;" />
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <span class="pull-right">{{ $firstname }}</span>
                                    <span>FastName</span>
                                </li>
                                <li class="list-group-item">
                                    <span class="pull-right">{{ $lastname }}</span>
                                    <span>LastName</span>
                                </li>
                                <li class="list-group-item">
                                    <span class="pull-right">{{ $username }}</span>
                                    <span>UserName</span>
                                </li>

                                <li class="list-group-item">
                                    <span class="pull-right">{{ $email }}</span>
                                    <span>Email</span>
                                </li>
                                <li class="list-group-item">
                                    <button class="bn btn-primary btn-block btn-lg">Change Password</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection