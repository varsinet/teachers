@extends('teach/master')
@section('content')
    <section id="main-content">
        <section class="wrapper" style="margin-top: 60px;">
            <div class="row">
                <div class="col-lg-12" ng-view>
                </div>
            </div>
        </section>
    </section>

    </section>
@endsection