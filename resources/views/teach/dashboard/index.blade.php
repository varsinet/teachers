@extends('teach/master')
@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="row state-overview">
                <div class="col-lg-4">

                    <section class="panel">
                        <div class="profile-widget profile-widget-info">
                            <div class="panel-body">
                                <div class="col-lg-4 col-sm-4 profile-widget-name">
                                    <h4>Fatt Kay</h4>
                                    <div class="follow-ava">
                                        <img src="img/avatar1_small.png" alt="">
                                    </div>
                                    <h6>Teacher</h6>
                                </div>
                                <div class="col-lg-8 col-sm-8 follow-info">
                                    <p>Welcome {{ $username }}, Please check your uploaded courses or upload new ones.</p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="state col-lg-8">
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <section class="panel">
                                <div class="symbol">
                                    <i class="icon_tags_alt"></i>
                                </div>
                                <div class="value">
                                    <h1>2</h1>
                                    <p>Courses</p>
                                </div>
                            </section>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <section class="panel">
                                <div class="symbol">
                                    <i class="icon_globe"></i>
                                </div>
                                <div class="value">
                                    <h1>14</h1>
                                    <p>Students</p>
                                </div>
                            </section>
                        </div>

                        <div class="col-lg-4 col-sm-6">
                            <section class="panel">
                                <div class="symbol">
                                    <i class="icon_currency"></i>
                                </div>
                                <div class="value">
                                    <h1>#5,500</h1>
                                    <p>Total Profit</p>
                                </div>
                            </section>
                        </div>
                    </div>


                </div>

            </div>

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            My Courses
                        </header>
                        @if(!empty($courses))
                        <table class="table table-striped border-top" id="sample_1">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Course Name
                                </th>
                                <th>
                                    Date Created
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    No. Of Registered Students
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($courses as $course)
                            <tr class="odd gradeX">
                                <td></td>
                                <td>
                                    <a href="{{ route('GetCourse',['link'=>$course['link']]) }}">{{ $course['title'] }}</a>
                                </td>
                                <td>
                                    {{
                                    \Carbon\Carbon::createFromTimestampUTC($course['createdAt']['sec'])->toDateString()
                                    }}
                                </td>
                                <td>
                                    @if($course['status'] == 0)
                                    <a href="#" class="btn btn-danger">Unactivated</a>
                                        @else
                                        <a href="#" class="btn btn-danger">Activated</a>
                                    @endif
                                </td>
                                <td>

                                </td>
                                <td>
                                    <a href="#" class="btn btn-primary">Edit</a>
                                    <a href="#" class="btn btn-primary">Delete</a>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                            <div>No courses</div>
                         @endif
                    </section>
                </div>
            </div>


        </section>
    </section>

    </section>
@endsection