@include('teach/include/header')
  <body ng-app="myVarsinet">
  <section id="container" class="">
      <header class="header white-bg">
            <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"></div>
            </div>
          <a href="index.php" class="logo">Varsi<span>net</span></a>
            <div class="top-nav notification-row">
                <ul class="nav pull-right top-menu">
                    <li id="alert_notificatoin_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="icon-bell-l"></i>
                            <span class="badge bg-important">1</span>
                        </a>
                        <ul class="dropdown-menu extended notification">
                            <div class="notify-arrow notify-arrow-blue"></div>
                            <li>
                                <p class="blue">You have 1 new notification(s)</p>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-primary"><i class="icon_profile"></i></span> 
                                    One New student
                                    <span class="small italic pull-right">6 hrs ago</span>
                                </a>
                            </li>
                                                     
                            <li>
                                <a href="#">See all notifications</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img alt="" src="img/avatar1_smalll.png">
                            </span>
                            <span class="username">{{ $username }}</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="#"><i class="icon_profile"></i> My Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon-bell-l"></i> Notifications</a>
                            </li>
                            
                            <li>
                                <a href="login.php"><i class="icon_key_alt"></i> Log Out</a>
                            </li>
                            
                        </ul>
                    </li>

                </ul>

            </div>
      </header>      
      <aside>
          <div id="sidebar"  class="nav-collapse ">

              <ul class="sidebar-menu">                
                  <li class="active">
                      <a class="" href="{{ route('teachDashboard') }}">
                          <i class="icon_house_alt"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
                  <!--
                  <li class="sub-menu">
                      <a href="javascript:;" class="">
                          <i class="icon_document_alt"></i>
                          <span>Courses</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
                      <ul class="sub">
                          <li><a class="" href="{{ route('teachCreateCourse') }}">Add New Course</a></li>
                          <li><a class="" href="{{ route('teachCreateCourse') }}">Edit Courses</a></li>
                       </ul>
                  </li>
                  -->
                  <li class="">
                      <a class="" href="{{ route('teachCreateCourse') }}">
                          <i class="icon_plus"></i>
                          <span>Add New Course</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="{{ route('teachProfile') }}" class="">
                          <i class="icon_profile"></i>
                          <span>Profile</span>
                      </a>
                  </li>
              </ul>
          </div>
      </aside>
    @yield('content')
  </section>
  @include('teach/include/footer');
  </body>
</html>