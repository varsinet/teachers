<?php
namespace App\Http\Controllers\Teach;

use App\Http\Controllers;
use App\Helpers\Contracts\MakeRequestContract;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controllers\Controller {

    public function login(CookieJar $cookieJar ,Request $request, MakeRequestContract $requestContract){
        $data = [];
        if($request->method() == 'POST'){
            $data = $this->validateInput($request);
            $response = $requestContract->post('/lecturer/login',$data);
            if($response->statusCode == 200){
                // var_dump($response->response);
                $user = $response->response['data'];
                if(isset($user['cookie'])){
                    $cookieJar->queue(cookie('sess_id',$user['cookie'],604800));
                }
                $request->session()->set('user',$user);
                return redirect('/teach/dashboard');
            } else if($response->statusCode == 400) {
                $data['error'] = 'Wrong email or password';
                //var_dump($response->error_response);
            }
        }
        return view('teach/login/index', $data);
    }

    public function validateInput(Request $request){
        $validator = Validator::make($request->all(),[
            'email'     => 'required',
            'password'  => 'required',
        ]);
        if($validator->fails()){
            return redirect('/login')
                ->withInput()
                ->withErrors($validator);
        } else {
            return [
                'email' => $request->input('email'),
                'password' => $request->input('password'),
                'remember' => $request->input('remember'),
            ];
        }
    }

    public function test(MakeRequestContract $make){
        $boom = $make->test();
        return view('dashboard.index',compact($boom));
    }
}