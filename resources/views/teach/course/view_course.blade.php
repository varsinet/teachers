@extends('teach/master')
@section('content')
    <section id="main-content">
        <section class="wrapper" style="margin-top: 60px;">
            <h2 class="text-center">{{ $course['title'] }}</h2>
            <div class="row state-overview">
                <div class="state col-lg-12">
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <section class="panel">
                                <div class="symbol">
                                    <i class="icon_tags_alt"></i>
                                </div>
                                <div class="value">
                                    <h1>{{ count($course['chapters']) }}</h1>
                                    <p>Chapters</p>
                                </div>
                            </section>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <section class="panel">
                                <div class="symbol">
                                    <i class="icon_globe"></i>
                                </div>
                                <div class="value">
                                    <h1>14</h1>
                                    <p>Topics</p>
                                </div>
                            </section>
                        </div>

                        <div class="col-lg-4 col-sm-6">
                            <section class="panel">
                                <div class="symbol">
                                    <i class="icon_currency"></i>
                                </div>
                                <div class="value">
                                    <h1>15</h1>
                                    <p>Registered Students</p>
                                </div>
                            </section>
                        </div>
                    </div>


                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            About
                        </div>
                        <div class="panel-body">
                            <p>{{ $course['description'] }}</p>
                    </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            Syllabus
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                            <table class="table table-bordered">

                            @for($i = 0 ; $i < count($course['chapters']); $i++)
                                <tr>
                                    <td>{{ $i + 1 }}</td>
                                    <td>
                                        <h3>{{ $course['chapters'][$i]['title'] }}</h3>
                                        <p>{{ $course['chapters'][$i]['description'] }}</p>

                                    @for($j = 0 ; $j < count($course['chapters'][$i]['topics']); $j++)
                                    <div class="list-group">
                                        <div class="list-group-item">
                                            <h4 class="list-group-item-heading">{{ $course['chapters'][$i]['topics'][$j]['title'] }}</h4>
                                            <p class="list-group-item-text"> {{ $course['chapters'][$i]['topics'][$j]['description'] }}</p>
                                        </div>
                                    </div>
                                        @endfor
                                    </td>
                                </tr>
                            @endfor
                            </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </section>
    </section>
@endsection