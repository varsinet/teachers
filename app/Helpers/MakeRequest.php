<?php

/**
 * Created by PhpStorm.
 * User: samparsky
 * Date: 4/17/16
 * Time: 11:15 AM
 */
namespace App\Helpers;

use App\Helpers\Contracts\MakeRequestContract;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

define('BASE_URL','http://localhost/varsinet/v1');

class MakeRequest implements MakeRequestContract
{
    private $client;
    public $statusCode;
    public $response;
    public $error_response;

    public function __construct()
    {
        //Guzzle Http Client
        $this->client = new Client([]);
    }

    public function get($uri , $data){
        try {
            $response = $this->client->request('GET',BASE_URL.$uri,[
                'query' => $data,
                ['http_errors' => true]
            ]);
            $this->statusCode = $response->getStatusCode();
            $this->response  = json_decode($response->getBody()->getContents(),true);
        } catch(ClientException $ex) {
            $this->statusCode = $ex->getCode();
            $this->error_response = json_decode($ex->getResponse()->getBody()->getContents(), true);
        }
        return $this;
    }

    public function test(){
        return 'Hello world';
    }

    public function post($uri , $data)
    {
        try {
            $response = $this->client->request('POST',BASE_URL.$uri,[
                'form_params' => $data,
                ['http_errors' => true]
            ]);
            $this->statusCode = $response->getStatusCode();
            $this->response   = json_decode($response->getBody()->getContents(),true);
        } catch (ClientException $ex){
            $this->statusCode = $ex->getCode();
            $this->error_response   = json_decode($ex->getResponse()->getBody()->getContents(),true);
        } finally {
            
        }
        return $this;
    }
    public function delete($uri , $data)
    {
        // TODO: Implement delete() method.
    }
    public function patch($uri , $data)
    {
        // TODO: Implement patch() method.
    }
    public function put($uri , $data)
    {
        // TODO: Implement put() method.
    }
}