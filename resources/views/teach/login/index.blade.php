@include('teach/include/header')
<body class="login-img2-body">

    <div class="container">

      <form class="login-form" method="post" style="margin-top:10%" action="{{ route('teachLogin') }}">
        <div class="login-wrap">
            {{ csrf_field() }}
		   <h3 align="center">Varsinet Login</h3>
            <p class="login-img"><i class="icon_lock_alt"></i></p>
            @include('teach.common.errors')
            @if(!empty($error))
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
            @endif
            <div class="input-group">
              <span class="input-group-addon"><i class="icon_profile"></i></span>
              <input type="email" name="email" class="form-control" placeholder="Email" autofocus>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <label class="checkbox">
                <input type="checkbox" name="remember" value="true"> Remember me
                <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
            </label>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
            <button class="btn btn-info btn-lg btn-block" type="button">Signup</button>
        </div>
      </form>

    </div>
    @include('teach/include/footer')
  </body>