/**
 * Created by samparsky on 4/20/16.
 */
var varsinetApp = angular.module('myVarsinet',['ngRoute','ngFileUpload'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

var BASE_URL = 'http://localhost/varsinet/v1';
/**
 ** DIRECTIVES
 *
 **/
varsinetApp.config(function($routeProvider){
    $routeProvider
        .when('/', {
            templateUrl : 'partials/create.html',
            controller  : 'CreateController'
        })
        .when('/course',{
            templateUrl : 'partials/course.html',
            controller  : 'CourseController'
        })
        .when('/topic',{
            templateUrl : 'partials/topic.html',
            controller  : 'TopicController'
        })
        .when('/quiz',{
            templateUrl : 'partials/quiz.html',
            controller  : 'QuizController'
        })
        .when('/finished', {
            templateUrl : 'partials/finished.html',
            controller : 'FinishedController'
        });
});

varsinetApp.factory('CourseData',function(){
    var courseData = {
        title : "", description : "",
        category : "", image :"",
        instructor : "", chapter : [],
    };

    return {
        data : function(){
            return courseData;
        },
        saveData : function(data){
            courseData = data;
        },
        getTitle : function(){
            return courseData.title;
        },
        setTitle : function(title){
            courseData.title = title;
        },
        getDescription : function(){
            return courseData.description;
        },
        setDescription : function(description){
            courseData.description = description;
        },
        getCategory : function(){
            return courseData.category;
        },
        setCategory : function(category){
            courseData.category = category;
        },
        setImage : function(image){
            courseData.image = image;
        },
        getImage : function(){
            return courseData.image;
        },
        setInstructor : function(instructor){
            courseData.instructor =  instructor;
        },
        setChapter : function(Chapter){
            courseData.chapter  = Chapter;
        },
        getChapter : function(){
            return courseData.chapter;
        },
        getTopic : function(chapter){
            return courseData.chapter[chapter].topic;
        },
        setTopic : function(chapter,topic){
            courseData.chapter[chapter].topic = topic;
        },
        getQuiz : function(chapter,topic){
            return courseData.chapter[chapter].topic[topic].quiz;
        },
        setQuiz : function(chapter,topic,quiz){
            courseData.chapter[chapter].topic[topic].quiz = quiz;
        }


    };
});

varsinetApp.controller('CreateController',function($scope,Upload,$http,$location,$timeout,CourseData){

    $scope.create = function(data){
        if ($scope.form.file.$valid) {
            $scope.uploadFile($scope.file);
        } else {
            alert('Please select an image');
            return;
        }

        CourseData.setTitle(data.title);
        CourseData.setDescription(data.description);
        CourseData.setCategory(data.category);

        /**
        $http.post(BASE_URL+'/course',data, []).then(function(data) {
            console.log(data);
        },function(error){
            console.log(error);
        });
         **/
        $location.path('/course');
    };

    $scope.uploadFile = function(file){
            Upload.upload({
                url: BASE_URL+'/course/upload',
                data: {upload: file}
            }).then(function (resp) {
                console.log('Success');
                //var response = JSON.parse(resp.data);
                console.log(resp.data);
                CourseData.setImage(resp.data.data.url);
                console.log(CourseData.data())
            }, function (resp) {
                console.log('Error status: ' + resp.status);
                console.log('Eror Data:' + resp.data.data);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
    };
});

varsinetApp.controller('CourseController',function($scope,$compile,$location,CourseData){

    $scope.count = 0;
    $scope.courseTitle = CourseData.getTitle();

    $scope.chapters = [{title : '', description : '',topic : []}];

    $scope.add = function(){
        $scope.count++;
        $scope.chapters.push({title: '',description: '',topic : []});
        console.log($scope.chapters);
    };

    $scope.remove = function(){
        $scope.count--;
        var lastItem = $scope.chapters.length-1;
        $scope.chapters.splice(lastItem);
        console.log($scope.chapters);
    };

    $scope.save = function(){
        CourseData.setChapter($scope.chapters);
        console.log(CourseData.data());
        $location.path('/topic');
    }
});


varsinetApp.controller('TopicController',function($scope,$location,CourseData){
    $scope.chapters = CourseData.getChapter();
    $scope.length = $scope.chapters.length - 1;
    $scope.chapter = 0;
    $scope.topics = [{title: '',description : '',video:'',image:'',quiz : []}];

    $scope.add = function(){
        $scope.topics.push({title: '',description : '',video:'',image:'',quiz: []});
        console.log($scope.topics);
    };

    $scope.next = function(){
            CourseData.setTopic($scope.chapter,$scope.topics);
            $scope.chapter++;
            console.log(CourseData.data());
        if($scope.chapter < ($scope.chapters.length)){
            $scope.topics = [{title: '',description : '',video:'',image:'',quiz : []}];
            return;
        } else {
            $location.path('/quiz');
        }
    };

    $scope.remove = function(){
        var lastItem = $scope.topics.length - 1;
        $scope.topics.splice(lastItem);
    };

    $scope.save = function(){
        console.log(CourseData.data());
        $location.path('/quiz');
    };
});

varsinetApp.controller('QuizController',function($scope,$location,CourseData){

    $scope.finished = false;
    $scope.topicFinished = true;

    $scope.courseIndex = 0;
    $scope.topicIndex = 0;

    $scope.chapters = CourseData.getChapter();
    $scope.topics = CourseData.getTopic($scope.courseIndex);

    $scope.chapterLength = $scope.chapters.length;
    $scope.topicLength   = $scope.topics.length;

    $scope.currentChapter = CourseData.getChapter()[$scope.courseIndex].title;
    $scope.currentTopic   = $scope.topics[$scope.topicIndex].title;

    $scope.quizzes = [{question:'',option:[],answer:''}];

    $scope.add = function(){
        $scope.quizzes.push({question: '',option : '',answer:''});
        console.log($scope.quizzes);
    };

    $scope.remove = function(){
        var lastItem = $scope.quizzes.length - 1;
        $scope.quizzes.splice(lastItem);
    };

    $scope.nextTopic  = function(){
        CourseData.setQuiz($scope.courseIndex,$scope.topicIndex,$scope.quizzes);
        $scope.topicIndex++;
        if($scope.topicIndex < ($scope.topicLength)){
            console.log(CourseData.data());
            $scope.quizzes = [{question:'',option:'',answer:''}];
        } else {
            $scope.topicFinished = false;
        }
    };

    $scope.nextChapter = function(){
        $scope.topicFinished = true;
        if($scope.courseIndex < $scope.chapterLength){
            $scope.courseIndex++;
        }
        $scope.quizzes = [{question:'',option:[],answer:''}];
        console.log($scope.courseIndex);
    };

    $scope.save = function(){
        console.log("Finished "+CourseData.data());
        $location.path('/');
    };
});

varsinetApp.controller('FinishedController',function($scope){

});