<?php
/**
 * Created by PhpStorm.
 * User: samparsky
 * Date: 4/20/16
 * Time: 9:52 AM
 */

namespace App\Http\Controllers\Teach;


use App\Helpers\Contracts\MakeRequestContract;
use App\Http\Controllers\Controller;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function dashboard(Request $request,MakeRequestContract $requestContract){
        $data = $request->session()->get('user');
        $courses = $requestContract->get('/lecturer/courses/'.$data['_id']['$id'],[]);

        if((int)$courses->statusCode ==  200) {
            $myCourses = $courses->response['data'];
            //var_dump($myCourses);
            //$request->session()->set('myCourses',$myCourses);
            $data['courses'] = $myCourses;
        }

        return view('teach/dashboard/index',$data);
    }

    public function course(Request $request){

    }

    public function convertDate(array $data){
        $time = new \DateTime($data['sec']);
        $timeString = $time->format('Y-m-d');
        return $timeString;
    }
}