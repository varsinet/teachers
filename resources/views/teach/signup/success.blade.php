@include('include/header')
<body>
    <div class="container">
        <div class="col-md-offset-4 col-md-4">
            <h1 class="text-center">Successful Registration</h1>
            <p>A confirmation message has been sent to your email
            </p>
        </div>
    </div>
</body>
@include('include/footer')